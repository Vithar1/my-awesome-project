import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:timeloggermobile/app/shared/colors.dart';
import 'package:timeloggermobile/app/shared/dimensions.dart';
import 'package:page_transition/page_transition.dart';

import 'login_view.dart';

class SignUpView extends StatelessWidget {

  final nameController = TextEditingController();
  final emailController = TextEditingController();
  final passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: darkColor,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          vSizedBox3,
          vSizedBox2,
          Container(
            color: darkColor,
            child: Row(
              children: [
                IconButton(icon: Icon(EvaIcons.arrowIosBackOutline, color: Colors.white,),
                onPressed: () {},)
              ],
            ),
          ),
          vSizedBox1,
          Container(
            color: Colors.black,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text("Hey there!",
                  style: TextStyle(
                    fontSize: 40,
                    fontWeight: FontWeight.w900,
                    color: whiteColor
                  ),
                ),
                Text("Welcome on the board",
                  style: TextStyle(
                    fontSize: 26,
                    fontWeight: FontWeight.w500,
                    color: whiteColor
                  ),
                ),
                Text("Fill in your details",
                  style: TextStyle(
                    fontSize: 26,
                    fontWeight: FontWeight.w500,
                    color: whiteColor
                  ),
                )
              ],
            ),
          ),
          vSizedBox3,
          vSizedBox1,
          Container(
            child: Column(
              children: [
                vSizedBox1,
                stylishTextField("Name", nameController),
                vSizedBox1,
                stylishTextField("Email", emailController),
                vSizedBox1,
                stylishTextField("Password", passwordController),
              ],
            ),
          ),
          vSizedBox4,
          Container(
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  RichText(text: TextSpan(children: <TextSpan>[
                    TextSpan(
                      text: "Already have an account? ",
                      style: TextStyle(
                        fontFamily: "Montserrat",
                        fontWeight: FontWeight.w700,
                        color: textColor
                      )
                    ),
                    TextSpan(
                      text: "Login",
                      recognizer: TapGestureRecognizer()..onTap = () {
                        Navigator.pushReplacement(context, PageTransition(
                          child: LoginView(), type: PageTransitionType.rightToLeft));
                      },
                      style: TextStyle(
                        fontFamily: "Montserrat",
                        fontWeight: FontWeight.bold,
                        color: textColor
                      )
                    )
                  ])),
                  vSizedBox2,
                  Container(
                    width: 300,
                    height: 50,
                    decoration: BoxDecoration(
                      color: Colors.white.withOpacity(1),
                      borderRadius: BorderRadius.circular(18)
                    ),
                    child: Center(
                      child: Text(
                        "Sign Up",
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w900,
                          fontFamily: "Montserrat",
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  stylishTextField(String text, TextEditingController textEditingController) {
    return TextField(
      controller: textEditingController,
      style: TextStyle(color: whiteColor, fontSize: 18.0),
      decoration: new InputDecoration(
        suffixIcon: IconButton(
          onPressed: () {},
          icon: Icon(EvaIcons.backspace, color: textColor), 
        ),
        filled: true,
        hintText: text,
        hintStyle: TextStyle(color: textColor, fontSize: 14.0),
        fillColor: bgColor,
        border: new OutlineInputBorder( 
          borderSide: BorderSide.none,
          borderRadius: const BorderRadius.all(
            Radius.circular(15.0)
          )
        )
      ),
    );
  }

}