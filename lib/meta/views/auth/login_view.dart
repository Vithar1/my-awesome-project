import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:timeloggermobile/app/shared/colors.dart';
import 'package:timeloggermobile/app/shared/dimensions.dart';
import 'package:timeloggermobile/meta/views/auth/signup_view.dart';
import 'package:page_transition/page_transition.dart';
import 'package:timeloggermobile/meta/views/companies_view.dart';
import 'package:timeloggermobile/meta/views/company_tasks_view.dart';
import 'package:timeloggermobile/providers/task_provider.dart';
import 'package:timeloggermobile/providers/user_provider.dart';

import '../../../api/google_signin_api.dart';
import '../../../domain/list_tasks.dart';
import '../../../domain/user.dart';
import '../../../providers/auth_provider.dart';
import '../logged_in.dart';

class LoginView extends StatelessWidget {
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  String password = "user1";

  @override
  Widget build(BuildContext context) {
    AuthProvider auth = Provider.of<AuthProvider>(context);

    Future signIn() async {
      final user = await GoogleSignInApi.login();

      if (user == null) {
        ScaffoldMessenger.of(context)
            .showSnackBar(const SnackBar(content: Text("Sign in failed")));
      } else {
        print(user.email);
        print(password);
        final Future<Map<String, dynamic>> successfulMessage =
            auth.login(user.email, password);
        successfulMessage.then((response) {
          if (response['status']) {
            User userLoggedIn = response['user'];
            Provider.of<UserProvider>(context, listen: false)
                .setUser(userLoggedIn);
            Navigator.of(context).pushReplacement(
              MaterialPageRoute(builder: (context) => CompaniesView() ),
            );
          } else {
            return showDialog<void>(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Text('Failed Login'),
                  content: Text(response.toString()),
                  actions: <Widget>[
                    TextButton(
                      child: Text('Ok'),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                  ],
                );
              },
            );
          }
        });
      }
    }

    Future signInLogin() async {
      print("BBBBBBBBBBBBBBBBBBBBBB");
      String _password = passwordController.value.text;
      String _email = emailController.value.text;
      final Future<Map<String, dynamic>> successfulMessage =
          auth.login(_email, _password);
      successfulMessage.then((response) {
        if (response['status']) {
          User userLoggedIn = response['user'];

          Provider.of<UserProvider>(context, listen: false)
              .setUser(userLoggedIn);

          Navigator.of(context).pushReplacement(
            MaterialPageRoute(builder: (context) => CompaniesView()),
          );
        } else {
          return showDialog<void>(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text('Failed Login'),
                content: Text(response.toString()),
                actions: <Widget>[
                  TextButton(
                    child: Text('Ok'),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              );
            },
          );
        }
      });
    }

    final googleBtn = new SizedBox(
      width: 300,
      height: 40,
      child: ElevatedButton.icon(
          onPressed: signIn,
          icon: FaIcon(
            FontAwesomeIcons.google,
            color: Colors.red,
          ),
          label: Text('Google')),
    );

    final loginBtn = new SizedBox(
      width: 300,
      height: 40,
      child: ElevatedButton.icon(
          onPressed: signInLogin,
          icon: FaIcon(FontAwesomeIcons.user, color: Colors.white),
          label: Text('Войти')),
    );

    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: backColor,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            color: backColor,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  "Авторизация",
                  style: TextStyle(
                      fontSize: 40,
                      fontWeight: FontWeight.w900,
                      color: whiteColor),
                ),
                vSizedBox2,
                Text(
                  "Рады вас видеть",
                  style: TextStyle(
                      fontSize: 26,
                      fontWeight: FontWeight.w500,
                      color: whiteColor),
                ),
                Text(
                  "Мы скучали!",
                  style: TextStyle(
                      fontSize: 26,
                      fontWeight: FontWeight.w500,
                      color: whiteColor),
                )
              ],
            ),
          ),
          vSizedBox3,
          vSizedBox1,
          Container(
            child: Column(
              children: [
                vSizedBox1,
                stylishTextField("Почта", emailController, false),
                vSizedBox1,
                stylishTextField("Пароль", passwordController, true),
              ],
            ),
          ),
          vSizedBox2,
          googleBtn,
          vSizedBox1,
          Container(
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  RichText(
                      text: TextSpan(children: <TextSpan>[
                    TextSpan(
                        text: "Войти с помощью Google",
                        style: TextStyle(
                            fontFamily: "Montserrat",
                            fontWeight: FontWeight.w700,
                            color: textColor)),
                  ])),
                  vSizedBox1,
                  loginBtn,
                  vSizedBox1,
                  RichText(
                      text: TextSpan(children: <TextSpan>[
                    TextSpan(
                        text: "Узнайте ваш логин и пароль у менеджера",
                        style: TextStyle(
                            fontFamily: "Montserrat",
                            fontWeight: FontWeight.w700,
                            color: textColor))
                  ])),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  stylishTextField(
      String text, TextEditingController textEditingController, bool param) {
    return TextField(
      obscureText: param,
      controller: textEditingController,
      style: TextStyle(color: whiteColor, fontSize: 18.0),
      decoration: new InputDecoration(
          suffixIcon: IconButton(
            onPressed: () {},
            icon: Icon(EvaIcons.backspace, color: textColor),
          ),
          filled: true,
          hintText: text,
          hintStyle: TextStyle(color: textColor, fontSize: 14.0),
          fillColor: new Color.fromRGBO(107, 73, 243, 1.0),
          border: new OutlineInputBorder(
              borderSide: BorderSide.none,
              borderRadius: const BorderRadius.all(Radius.circular(15.0)))),
    );
  }
}
