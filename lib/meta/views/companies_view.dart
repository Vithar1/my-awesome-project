import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:timeloggermobile/domain/list_companies.dart';
import 'package:timeloggermobile/meta/views/company_tasks_view.dart';
import 'package:timeloggermobile/meta/views/projects_view.dart';
import 'package:timeloggermobile/providers/company_provider.dart';

import '../../domain/user.dart';
import '../../providers/user_provider.dart';

class CompaniesView extends StatefulWidget {

  @override
  State<CompaniesView> createState() => _CompaniesViewState();
}

class _CompaniesViewState extends State<CompaniesView> {
  @override
  Widget build(BuildContext context) {
    CompanyProvider companyProvider = Provider.of<CompanyProvider>(context);
    late User user = Provider.of<UserProvider>(context).user;
    late CompaniesList companiesList = CompaniesList();

    Future<String> getCompanies() async {
      final Future<Map<String, dynamic>> successfulMessage =
      companyProvider.getAllCompanies(user.jwt!);
      successfulMessage.then((response) {
        setState(() {
          companiesList = response['companies'];
          companyProvider.setCompany(companiesList);
        });
      });
      print("1111111");
      print(companyProvider.company.companies.toString());
      return "easy";
    }


    return FutureBuilder(
        future: getCompanies(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Scaffold(
              backgroundColor: Colors.grey,
              appBar: AppBar(
                backgroundColor: Colors.blue,
                title: Text("Компании"),
                centerTitle: true,
                elevation: 0,
              ),
              body: ListView.builder(
                  itemCount: 2,
                  itemBuilder: (context, index) {
                    return Card(
                      child: ListTile(
                        onTap: () {
                          Navigator.of(context).pushReplacement(
                            MaterialPageRoute(builder: (context) => ProjectsView()),
                          );
                        },
                        title: Text(
                            "${companyProvider.company.companies![index].name}"),
                      ),
                    );
                  }),
            );
          } else {
            return Center(child: CircularProgressIndicator());
          }
        });
  }
}
