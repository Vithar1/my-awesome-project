import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:timeloggermobile/domain/list_project.dart';
import 'package:timeloggermobile/providers/project_provider.dart';

import '../../domain/user.dart';
import '../../providers/user_provider.dart';
import 'company_tasks_view.dart';

class ProjectsView extends StatefulWidget {

  @override
  State<ProjectsView> createState() => _ProjectsViewState();
}

class _ProjectsViewState extends State<ProjectsView> {
  @override
  Widget build(BuildContext context) {
    ProjectProvider projectProvider = Provider.of<ProjectProvider>(context);
    late User user = Provider.of<UserProvider>(context).user;
    late ProjectsList projectsList = ProjectsList();

    Future<String> getProjects() async {
      final Future<Map<String, dynamic>> successfulMessage =
      projectProvider.getAllProjects(user.jwt!);
      successfulMessage.then((response) {
        setState(() {
          projectsList = response['projects'];
          projectProvider.setProject(projectsList);
        });
      });
      print("BLYAEAAEAAA");
      print(projectProvider.projectList.projects.toString());
      return "easy";
    }

    return FutureBuilder(
        future: getProjects(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Scaffold(
              backgroundColor: Colors.grey,
              appBar: AppBar(
                backgroundColor: Colors.blue,
                title: Text("Проекты"),
                centerTitle: true,
                elevation: 0,
              ),
              body: ListView.builder(
                  itemCount: 1,
                  itemBuilder: (context, index) {
                    return Card(
                      child: ListTile(
                        onTap: () {
                          Navigator.of(context).pushReplacement(
                            MaterialPageRoute(builder: (context) => CompanyTasksView()),
                          );
                        },
                        title: Text(
                            "${projectProvider.projectList.projects![index].name}"),
                      ),
                    );
                  }),
            );
          } else {
            return Center(child: CircularProgressIndicator());
          }
        });
  }
}
