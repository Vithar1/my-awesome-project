import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:timeloggermobile/providers/user_provider.dart';

import '../../providers/timelog_provider.dart';

class TimelogView extends StatefulWidget {
  @override
  State<TimelogView> createState() => _TimelogViewState();
}

class _TimelogViewState extends State<TimelogView> {
  final formKey = GlobalKey<FormState>();
  final descriptionTextController = TextEditingController();
  final nameTextController = TextEditingController();

  late String _amountHours, _description;

  @override
  Widget build(BuildContext context) {
    TimelogProvider timelogProvider = Provider.of<TimelogProvider>(context);
    UserProvider userProvider = Provider.of<UserProvider>(context);

    create() {
      final form = formKey.currentState;
      if(form!.validate()) {
        form.save();
        timelogProvider.create(_amountHours, _description, userProvider.user);
      }
    }

    final amountHourdField = TextFormField(
        controller: nameTextController,
        autofocus: false,
        validator: (value) =>
        value!.isEmpty ? "Please fill name of event" : null,
        onSaved: (value) => _amountHours = value!,
        decoration: InputDecoration(
            border:
            OutlineInputBorder(borderRadius: BorderRadius.circular(5.0)),
            contentPadding:
            EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
            hintText: 'Название нового мероприятия'));

    final descriptionField = Container(
        width: 500,
        height: 100,
        child: TextFormField(
            controller: descriptionTextController,
            autofocus: false,
            expands: true,
            minLines: null,
            maxLines: null,
            onSaved: (value) => _description = value!,
            decoration: InputDecoration(
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5.0)),
                contentPadding:
                EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
                hintText: 'Описание проделанной работы')));

    final saveButtonsRow = Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Padding(
          padding: const EdgeInsets.fromLTRB(32.0, 8.0, 8.0, 8.0),
          child: MaterialButton(
            onPressed: () => create(),
            textColor: Colors.white,
            color: Color(0xFF0069C0),
            child: SizedBox(
              width: 50,
              child: Text(
                'Готово',
                textAlign: TextAlign.center,
              ),
            ),
            height: 30,
            minWidth: 50,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(5))),
          ),
        ),
      ],
    );

    return SafeArea(
        child: Scaffold(
      body: Center(
        child: SingleChildScrollView(
          child: Container(
              padding: EdgeInsets.all(5.0),
              child: Form(
                key: formKey,
                child: Column(
                  children: [
                    descriptionField,
                    SizedBox(
                      height: 5.0,
                    ),
                    amountHourdField,
                    SizedBox(height: 5.0),
                    saveButtonsRow
                  ],
                ),
              )),
        ),
      ),
    ));
  }
}
