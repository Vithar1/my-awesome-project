import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:timeloggermobile/meta/views/auth/signup_view.dart';

import '../../domain/user.dart';
import '../../providers/user_provider.dart';

class LoggedInPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    late User user = Provider.of<UserProvider>(context).user;
    print("LoggedInPage");
    print(user);
    return Scaffold(
      appBar: AppBar(
        title: Text('Logged IN'),
        centerTitle: true,
        actions: [
          TextButton(onPressed: () async {
            Navigator.of(context).pushReplacement(
                MaterialPageRoute(builder: (context) => SignUpView()));
          }, child: Text("Logout"))
        ],
      ),
      body: Container(
        alignment: Alignment.center,
        color: Colors.blueGrey.shade900,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('Profile', style: TextStyle(fontSize: 24)),
            SizedBox(height: 32),
            // CircleAvatar(
            //   radius: 40,
            //   backgroundImage: NetworkImage(user.photoUrl ?? "https://101kote.ru/upload/medialibrary/338/2.jpg"),
            // ),
            Text(
              'Name ' + user.surname!,
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
            SizedBox(height: 8,),
            Text(
                'Email ' + user.name!,
                style: TextStyle(color: Colors.white, fontSize: 20))
          ],
        ),
      ),
    );
  }
}
