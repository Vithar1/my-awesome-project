import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:timeloggermobile/app/shared/dimensions.dart';
import 'package:timeloggermobile/domain/list_tasks.dart';
import 'package:timeloggermobile/meta/views/timelog_view.dart';
import 'package:timeloggermobile/providers/task_provider.dart';
import 'package:timeloggermobile/providers/user_provider.dart';

import '../../app/shared/colors.dart';
import '../../domain/user.dart';

class CompanyTasksView extends StatefulWidget {
  @override
  _CompanyTaskState createState() => _CompanyTaskState();
}

class _CompanyTaskState extends State<CompanyTasksView> {
  @override
  Widget build(BuildContext context) {
    TaskProvider taskProvider = Provider.of<TaskProvider>(context);
    late User user = Provider.of<UserProvider>(context).user;
    late TasksList tasksList = TasksList();

    Future<String> getTasks() async {
      final Future<Map<String, dynamic>> successfulMessage =
          taskProvider.getAllTasks(user.jwt!);
      successfulMessage.then((response) {
        setState(() {
          tasksList = response['tasks'];
          taskProvider.setTask(tasksList);
        });
      });
      return "easy";
    }

    return FutureBuilder(
        future: getTasks(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Scaffold(
              backgroundColor: Colors.grey,
              appBar: AppBar(
                backgroundColor: Colors.blue,
                title: Text("${taskProvider.taskProvider.tasks![0].project}"),
                centerTitle: true,
                elevation: 0,
              ),
              body: ListView.builder(
                  itemCount: 2,
                  itemBuilder: (context, index) {
                    return Card(
                      child: ListTile(
                        onTap: () {
                          Navigator.of(context).pushReplacement(
                            MaterialPageRoute(builder: (context) => TimelogView()),
                          );
                        },
                        title: Text(
                            "${taskProvider.taskProvider.tasks![index].title}"),
                      ),
                    );
                  }),
            );
          } else {
            return Center(child: CircularProgressIndicator());
          }
        });
  }
}
