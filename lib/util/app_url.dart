class ApiUrl {
  static const String productionBaseUrl = 'https://my-awesome-timeloger.herokuapp.com';
  static const String localBaseUrl = 'http://10.0.2.2:8080';

  static const String baseURL = productionBaseUrl;
  static const String login = baseURL + '/api/authenticate';
  static const String getAccount = baseURL + '/api/account';
  static const String register = baseURL + "/register";
  static const String companies = baseURL + "/api/companies";
  static const String getTasks = baseURL + "/api/tasks";
  static const String projects = baseURL + "/api/projects";
  static const String timelogs = baseURL + "/api/daily-timelogs";
}