import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:timeloggermobile/meta/views/companies_view.dart';
import 'package:timeloggermobile/meta/views/company_tasks_view.dart';
import 'package:timeloggermobile/meta/views/projects_view.dart';
import 'package:timeloggermobile/meta/views/splash_screen/splash_view.dart';
import 'package:timeloggermobile/meta/views/timelog_view.dart';
import 'package:timeloggermobile/providers/auth_provider.dart';
import 'package:timeloggermobile/providers/company_provider.dart';
import 'package:timeloggermobile/providers/project_provider.dart';
import 'package:timeloggermobile/providers/task_provider.dart';
import 'package:timeloggermobile/providers/timelog_provider.dart';
import 'package:timeloggermobile/providers/user_provider.dart';
import 'package:timeloggermobile/util/shared_preference.dart';

import 'domain/user.dart';
import 'meta/views/auth/login_view.dart';
import 'meta/views/logged_in.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    Future<String> getJwt() => UserPreferences().getJwt();
    Future<User> getUser() => UserPreferences().getUser();

    return MultiProvider(providers: [
      ChangeNotifierProvider(create: (_) => AuthProvider()),
      ChangeNotifierProvider(create: (_) => UserProvider()),
      ChangeNotifierProvider(create: (_) => TaskProvider()),
      ChangeNotifierProvider(create: (_) => CompanyProvider()),
      ChangeNotifierProvider(create: (_) => ProjectProvider()),
      ChangeNotifierProvider(create: (_) => TimelogProvider())
    ],
      child: MaterialApp(
        theme: ThemeData(
            fontFamily: 'Montserrat'
        ),
        debugShowCheckedModeBanner: false,
        home: FutureBuilder(
          future: getJwt(),
          builder: (context, snapshot) {
            if (!snapshot.hasData) return SplashView();
            if (snapshot.data != "") {
              String data = snapshot.data.toString();
              var jwt = data.split(".");
              if (jwt.length != 3) {
                return LoginView();
              } else {
                var decode = json.decode(
                    ascii.decode(base64.decode(base64.normalize(jwt[1]))));
                if (DateTime.fromMicrosecondsSinceEpoch(decode["exp"] * 1000)
                    .isAfter(DateTime.now())) {
                  return LoginView();
                } else {
                  return LoginView();
                }
              }
            } else {
              return LoginView();
            }
          }
        ),
        routes: {
          '/authenticate': (context) => LoginView(),
          '/signin' : (context) => LoggedInPage(),
          '/tasks' : (context) => CompanyTasksView(),
          '/companies' : (context) => CompaniesView(),
          '/projects' : (context) => ProjectsView(),
          '/timelogs' : (context) => TimelogView(),
        },
      ),


    );

  }
}
