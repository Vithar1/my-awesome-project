import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:http/http.dart';

import '../domain/list_tasks.dart';
import '../domain/task.dart';
import '../util/app_url.dart';

class TaskProvider with ChangeNotifier {
  late TasksList _taskProvider;

  TasksList get taskProvider => _taskProvider;

  void setTask(TasksList taskProvider) {
    _taskProvider = taskProvider;
    notifyListeners();
  }

  Future<Map<String, dynamic>> getAllTasks(String token) async {
    var result;

    var response = await get(
      Uri.parse(ApiUrl.getTasks),
      headers: {'Authorization': 'Bearer $token'},
    ).timeout(const Duration(seconds: 10));

    notifyListeners();

    if (response.statusCode == 200) {
      var responseBody = jsonDecode(utf8.decode(response.bodyBytes));
      var responseList = TasksList.fromJson(responseBody);

      result = {'status': true, 'message': 'Successful', 'tasks': responseList};
    } else {
      result = {
        'status': false,
        'message': json.decode(response.body)['error']
      };
    }
    return result;
  }
}
