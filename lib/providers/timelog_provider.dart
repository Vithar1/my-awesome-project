import 'dart:convert';
import 'dart:core';

import 'package:flutter/cupertino.dart';
import 'package:http/http.dart';
import 'package:timeloggermobile/providers/user_provider.dart';

import '../domain/Timelog.dart';
import '../domain/user.dart';
import '../util/app_url.dart';

class TimelogProvider with ChangeNotifier {
  Timelog _timeLog = Timelog();
  Timelog get timelog =>_timeLog;

  void setTimelog(Timelog timelog) {
    _timeLog = timelog;
    notifyListeners();
  }

  Future<Response> create(String title, String description, User user) async{

    final Map<String, dynamic> creationData = {
      'title' : "Задача 1",
      'description' : description,
      'task' : '1252',
      'users' : {
        "id" : user.id
      },
      'date' : "2022-06-08T03:51:36.459Z"
    };

    return await post(
        Uri.parse(ApiUrl.timelogs),
        body: json.encode(creationData),
        headers: {'Content-Type': 'application/json', 'Authorization' : 'Bearer ${user.jwt}'}, )
        .timeout(const Duration(seconds: 3));
  }



}