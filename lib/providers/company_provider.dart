import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:http/http.dart';
import 'package:timeloggermobile/domain/company.dart';
import 'package:timeloggermobile/domain/list_companies.dart';

import '../util/app_url.dart';

class CompanyProvider with ChangeNotifier {
  late CompaniesList _company;

  CompaniesList get company => _company;

  void setCompany(CompaniesList company) {
    _company = company;
    notifyListeners();
  }

  Future<Map<String, dynamic>> getAllCompanies(String token) async {
    var result;

    var response = await get(
      Uri.parse(ApiUrl.companies),
      headers: {'Authorization': 'Bearer $token'},
    ).timeout(const Duration(seconds: 10));

    notifyListeners();

    if (response.statusCode == 200) {
      var responseBody = jsonDecode(utf8.decode(response.bodyBytes));
      var responseList = CompaniesList.fromJson(responseBody);

      result = {'status': true, 'message': 'Successful', 'companies': responseList};
    } else {
      result = {
        'status': false,
        'message': json.decode(response.body)['error']
      };
    }
    return result;
  }
}