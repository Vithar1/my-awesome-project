import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:http/http.dart';
import 'package:timeloggermobile/domain/user.dart';
import 'package:timeloggermobile/util/shared_preference.dart';

import '../util/app_url.dart';

enum Status {
  NotLoggedIn,
  NotRegistered,
  LoggedIn,
  Registered,
  Authenticating,
  Registering,
  LoggedOut
}

class AuthProvider with ChangeNotifier {
  Status _loggedInStatus = Status.NotLoggedIn;
  Status _registeredInStatus = Status.NotRegistered;
  Status get loggedInStatus => _loggedInStatus;
  Status get registeredInStatus => _registeredInStatus;

  Future<Map<String, dynamic>> login(String email, String password) async {
    var result;

    final Map<String, dynamic> loginData = {
        'username': email,
        'password': password
    };

    _loggedInStatus = Status.Authenticating;
    notifyListeners();
    var response = await post(
      Uri.parse(ApiUrl.login),
      body: json.encode(loginData),
      headers: {'Content-Type': 'application/json'},)
        .timeout(const Duration(seconds: 10));

    _loggedInStatus = Status.NotLoggedIn;
    notifyListeners();

    var token = jsonDecode(response.body)['id_token'];

    var userResponse = await get(
      Uri.parse(ApiUrl.getAccount),
      headers: {'Authorization' : 'Bearer $token'}
    ).timeout(const Duration(seconds: 10));


    if(userResponse.statusCode == 200) {
      var responseBody = jsonDecode(utf8.decode(userResponse.bodyBytes));
      var authUser = User.fromJson(responseBody, token);

      UserPreferences().saveUser(authUser);

      _loggedInStatus = Status.LoggedIn;
      notifyListeners();

      result = {'status': true, 'message': 'Successful', 'user': authUser};
    }else {
      _loggedInStatus = Status.NotLoggedIn;
      notifyListeners();
      result = {
        'status': false,
        'message': json.decode(response.body)['error']
      };
      }
    return result;
  }
}