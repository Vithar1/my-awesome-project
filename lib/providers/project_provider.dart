import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:http/http.dart';

import '../domain/list_project.dart';
import '../util/app_url.dart';

class ProjectProvider with ChangeNotifier {
  late ProjectsList _projectList;

  ProjectsList get projectList => _projectList;

  void setProject(ProjectsList project) {
    _projectList = project;
    notifyListeners();
  }

  Future<Map<String, dynamic>> getAllProjects(String token) async {
    var result;

    var response = await get(
      Uri.parse(ApiUrl.projects),
      headers: {'Authorization': 'Bearer $token'},
    ).timeout(const Duration(seconds: 10));

    notifyListeners();

    if (response.statusCode == 200) {
      var responseBody = jsonDecode(utf8.decode(response.bodyBytes));
      var responseList = ProjectsList.fromJson(responseBody);

      result = {'status': true, 'message': 'Successful', 'projects': responseList};
    } else {
      result = {
        'status': false,
        'message': json.decode(response.body)['error']
      };
    }
    return result;
  }

}