class Project {
  int? id;
  String? name;
  String? company;

  Project({this.id, this.name, this.company});

  factory Project.fromJson(Map<String, dynamic> responseData) {
    return Project(
      id: responseData['id'] as int,
      name: responseData['name'] as String,
      company: responseData['company']['name'] as String,
    );
  }
}