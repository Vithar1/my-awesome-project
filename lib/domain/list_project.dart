import 'package:timeloggermobile/domain/project.dart';

class ProjectsList {
  List<Project>? projects;

  ProjectsList({this.projects});

  factory ProjectsList.fromJson(List<dynamic> responseData) {
    List<Project> projects = List<Project>.from(responseData.map((i) => Project.fromJson(i)));
    return ProjectsList(
      projects: projects,
    );
  }
}