class Task {
  int? id;
  String? title;
  String? description;
  String? project;
  String? type;
  int? state;

  Task({this.id, this.title, this.description, this.project, this.type, this.state});

  factory Task.fromJson(Map<String, dynamic> responseData) {
    return Task(
      id: responseData['id'] as int,
      title: responseData['title'] as String,
      description: responseData['description'] as String,
      project: responseData['project']['name'] as String,
      type: responseData['type']['title'] as String,
      state: responseData['state'] as int,
    );
  }
}