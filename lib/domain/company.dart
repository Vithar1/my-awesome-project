class Company {
  int? id;
  String? name;
  int? creatorId;

  Company({this.id, this.name, this.creatorId});

  factory Company.fromJson(Map<String, dynamic> responseData) {
    return Company(
      id: responseData['id'] as int,
      name: responseData['name'] as String,
      creatorId: responseData['creator']['id'] as int,
    );
  }
}