class User {
  int? id;
  String? name;
  String? surname;
  String? jwt;

  User({this.id, this.name, this.surname, this.jwt});

  factory User.fromJson(Map<String, dynamic> responseData, String token) {
    return User(
        id: responseData['id'] as int,
        name: responseData['firstName'] as String,
        surname: responseData['lastName'] as String,
        jwt: token,
    );
  }
}