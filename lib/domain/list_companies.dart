import 'company.dart';

class CompaniesList {
  List<Company>? companies;

  CompaniesList({this.companies});

  factory CompaniesList.fromJson(List<dynamic> responseData) {
    List<Company> companies = List<Company>.from(responseData.map((i) => Company.fromJson(i)));
    return CompaniesList(
      companies: companies,
    );
  }
}