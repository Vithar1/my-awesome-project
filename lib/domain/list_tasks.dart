import 'package:timeloggermobile/domain/task.dart';

class TasksList {
  List<Task>? tasks;

  TasksList({this.tasks});

  factory TasksList.fromJson(List<dynamic> responseData) {
    List<Task> tasks = List<Task>.from(responseData.map((i) => Task.fromJson(i)));
    return TasksList(
      tasks: tasks,
    );
  }
}