class Timelog {
  int? id;
  String? title;
  String? description;

  Timelog({this.id, this.title, this.description});

  factory Timelog.fromJson(Map<String, dynamic> responseData) {
    return Timelog(
      id: responseData['id'] as int,
      title: responseData['title'] as String,
      description: responseData['description'] as String,
    );
  }

}